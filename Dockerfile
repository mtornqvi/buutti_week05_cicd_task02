# Local build : docker build -t mikko-ci-app --build-arg APP_PORT=3000 .
# Local test : winpty docker run -p 3000:3000 -it mikko-ci-app

# First stage: build and test
FROM node:16 as nodebuild

WORKDIR /usr/src/app
COPY package*.json ./
COPY ./src ./src
COPY ./test ./test
# public/index.html => static page
COPY ./public ./public
RUN npm install && npm run test

# Second stage: assemble the runtime image
# If first stage tests fail, second stage won't start
# Copy files from first stage
FROM nodebuild as noderun     
ARG APP_PORT
EXPOSE $APP_PORT

CMD ["npm","start"]