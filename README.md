## Azure address:
https://mikko-ci-app.azurewebsites.net/

Responds also to :
GET https://mikko-ci-app.azurewebsites.net/api/v1/books
GET https://mikko-ci-app.azurewebsites.net/api/v1/books/{0,1}
POST https://mikko-ci-app.azurewebsites.net/api/v1/books/
(etc.)

# Learned skills during this project
How to write Dockerfile with tests
How to write CI/CD Gitlab .yml file with development to Azure

# Unclear things
Azure CI/CD development still unclear, how feasible is it?
Development cycle seems to be at least 45 minutes, no automatic update?
